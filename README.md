# Apertis Snapshots Configuration Service

The Apertis Snapshots Configuration Service provides both a backend and a frontend to
configure Apertis snapshots.

The backend provides a REST API that can be used to retrieve and configure Apertis snapshots,
while the frontend can be used to interact with the backend in a user-friendly way.

The frontend uses the REST API to communicate with the backend, which should give more
flexibility on deployments.

# Testing

## Backend

### Development test

It is possible to test the backend locally during development, by running (inside the backend/ dir):
```
$ docker-compose up
```

This allows to connect to the backend using http://127.0.0.1:29080.

After stopping the backend using `Ctrl-c`, it is possible to remove the test container and
image by running:
```
$ docker container rm backend_apertis-snapshots-service_1
$ docker image rm registry.gitlab.apertis.org/infrastructure/apertis-snapshots-service/backend
```

#### User database

If not passing `--disable-authentication` (default when using the provided `docker-compose.override.yml`)
when starting the service, the backend will use a local user database for authentication, which is
then used to retrieve a Bearer authentication token needed to access its endpoints:

A new user can be setup by running the provided `create-user` script, for example:
```
$ docker exec -it backend_apertis-snapshots-service_1 /app/create-user --username "user" --password "user" --name "user" --email "user@user"
```

To retrieve a Bearer token for a user, do:
```
$ curl --user <username>:<password> http://<service_url>/v1/users/<user_id>
```

With \<username\> and \<password\> being the same ones passed to `create-user` above, which
in turn should return the \<user_id\> for the new user.

*Note that if `--disable-authentication` is used the token can be any arbitrary value and won't
be validated*.

#### REST APIs

Once with the token, the various endpoints can be accessed, for example:

1. To retrieve the list of snapshot configurations for \<prefix\>/\<distribution\> (where
\<distribution\> can be a regexp), run:
```
$ curl -H "Authorization: Bearer <bearer_token>" \
    "http://<service_url>/v1/snapshots/config/list?prefix=<prefix>&distribution=<distribution>"
```

2. To retrieve the snapshot configuration for \<prefix\>/\<distribution\>, run:
```
$ curl -H "Authorization: Bearer <bearer_token>" \
    "http://<service_url>/v1/snapshots/config/get?prefix=<prefix>&distribution=<distribution>"
```

3. To delete the snapshot configuration for \<prefix\>/\<distribution\>, run:
```
$ curl -H "Authorization: Bearer <bearer_token>" \
    -X DELETE \
    "http://<service_url>/v1/snapshots/config/delete?prefix=<prefix>&distribution=<distribution>"
```

4. To configure a snapshot for \<prefix\>/\<distribution\> to keep all snapshots indefinitely, run:
```
$ curl -H "Authorization: Bearer <bearer_token>" \
    -H "Content-Type: application/json" \
    -d '{"prefix": "<prefix>", "distribution": "<distribution>", "retention": { "keep": ["*"], "days_range": 14 } }' \
    "http://<service_url>/v1/snapshots/config/configure"
```

## Frontend

### Development test

It is possible to test the frontend locally during development.

Given the backend runs in its own container, to properly connect to the backend an env variable
must be passed when starting the frontend. This can be done by creating an `.env` file (e.g. `.env.dev`)
and setting `SNAPSHOTS_SERVICE_URL` to the url for the backend, for example:
```
SNAPSHOTS_SERVICE_URL=http://192.168.68.101:29080
```

And then starting the frontend container with:
```
$ docker-compose --env-file=.env.dev up
```

This allows to connect to the frontend using http://127.0.0.1:29088.

After stopping the frontend using `Ctrl-c`, it is possible to remove the test container and
image by running:
```
$ docker container rm frontend_apertis-snapshots-service_1
$ docker image rm registry.gitlab.apertis.org/infrastructure/apertis-snapshots-service/frontend
```

#### Connecting to backend

By default the frontend is started with `--disable-authentication` (when using the provided
`docker-compose.override.yml`) and will use a local user for development. To enable gitlab
authentication the following env variables must be set:
- `APP_SECRET_KEY`: any arbitrary value
- `GITLAB_OAUTH_CLIENT_ID`: the client id as configured in gitlab
- `GITLAB_OAUTH_CLIENT_SECRET`: the client secret as configured in gitlab

Also, if needed the gitlab url can be tweaked on `secrets/config.yaml` by adding
a `gitlab-url` entry with the url of the gitlab instance to connect to.
The `gitlab-url` defaults to `https://gitlab.apertis.org`.
