# Apertis Snapshots Configuration Service.
#
# Copyright (C) 2022 Collabora Ltd
# Andre Moreira Magalhaes <andre.magalhaes@collabora.com>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US

from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy

import os


app = Flask(__name__)

# Database configuration is received from the environment variables
# defined in the docker-compose file.
database_config = {
    "db_path": os.getenv("DB_PATH"),
}
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///%(db_path)s" % database_config
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.secret_key = os.getenv("APP_SECRET_KEY")

cors = CORS(app)
db = SQLAlchemy(app)

from .models import users
from .routes import routes
