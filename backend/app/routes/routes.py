# Apertis Snapshots Configuration Service.
#
# Copyright (C) 2022 Collabora Ltd
# Andre Moreira Magalhaes <andre.magalhaes@collabora.com>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US

from app import app
from app.helpers import token_auth, basic_auth
from app.models import get_user_by_id
from flask import jsonify, request

import logging
import re


# TODO: use actual data
snapshots = {
    "apertis:v2021":  {
        "prefix": "apertis",
        "distribution": "v2021",
        "retention": {
            "keep": ["*"]
        }
    },
    "apertis:v2022":  {
        "prefix": "apertis",
        "distribution": "v2022",
        "retention": {
            "keep": ["*"]
        }
    },
    "apertis:v2023dev*": {
        "prefix": "apertis",
        "distribution": "v2023dev*",
        "retention": {
            "keep": [
                "v2023dev1/20220607T080952Z",
                "v2023dev2/20220613T134012Z",
            ],
            "days_range": 7,
            "weeklies_range": 12,
            "monthlies_range": 6,
        },
    }
}

@app.route("/v1/users/<id>", methods=["GET"])
@basic_auth.login_required
def get_user_info(id):
    logging.debug(f"Retrying user info for user with id '{id}'")
    user = get_user_by_id(id)
    if user:
        return jsonify({
            "id": user.id,
            "username": user.username,
            "name": user.name,
            "email": user.email,
            "access_token_type": "bearer",
            "access_token": user.token.decode("utf-8")
        })

    return jsonify({}), 404

# The prefix and distribution must be set as url params
# (e.g. http://<service_url>/v1/snapshots/config/list?prefix=apertis&distribution=v2022).
#
# The prefix must be an exact match against the available snapshots.
# The distribution param also supports regular expressions
# (e.g. http://<service_url>/v1/snapshots/config/list?prefix=apertis&distribution=.*).
@app.route("/v1/snapshots/config/list", methods=["GET"])
@token_auth.login_required
def get_snapshots_list():
    global snapshots

    try:
        # TODO: validate input (must be strings)
        prefix = request.args["prefix"]
        distribution = request.args["distribution"]
    except:
        return jsonify({
            "message": "Missing mandatory 'prefix' and/or 'distribution' parameter"
        }), 400

    snapshot_id = f"{prefix}:{distribution}"
    logging.info(f"Retrieving snapshots configuration for {snapshot_id}")

    # TODO: actually retrieve snapshot configuration list

    # First check if we can find an exact match configuration
    # otherwise check each configuration distribution against the
    # request distribution in case the requested distribution is a
    # regular expression
    filtered_snapshots = []
    if snapshot_id in snapshots:
        filtered_snapshots.append(snapshots.get(snapshot_id))
    else:
        filtered_snapshots = [ snapshot for snapshot in snapshots.values() \
            if snapshot["prefix"] == prefix and re.fullmatch(distribution, snapshot["distribution"]) ]

    return jsonify({
        "snapshots": filtered_snapshots
    })

# The prefix and distribution must be set as url params
# (e.g. http://<service_url>/v1/snapshots/config/get?prefix=apertis&distribution=v2022).
#
# Both prefix and distribution must be an exact match against the available snapshots.
@app.route("/v1/snapshots/config/get", methods=["GET"])
@token_auth.login_required
def get_snapshot():
    global snapshots

    try:
        # TODO: validate input (must be strings)
        prefix = request.args["prefix"]
        distribution = request.args["distribution"]
    except:
        return jsonify({
            "message": "Missing mandatory 'prefix' and/or 'distribution' parameter"
        }), 400

    snapshot_id = f"{prefix}:{distribution}"
    logging.info(f"Retrieving snapshots configuration for {snapshot_id}")

    # TODO: actually retrieve snapshot configuration list

    if snapshot_id not in snapshots:
        return jsonify({
            "message": f"Could not delete configuration for snapshot {snapshot_id}: not found"
        }), 404

    snapshot = snapshots[snapshot_id]

    return jsonify({
        "snapshot": snapshot
    })

# Input should be as follows (json format):
# {
#   "prefix": "apertis",
#   "distribution": "v2023dev2",
#   "retention": {
#       "days_range": 3,
#       "weeklies_range": 5,
#       "monthlies_range": 7
#   },
#   "keep": [ "v2023dev2/20220525T122003Z", "v2023dev2/20220525T122003Z" ]
# }
@app.route("/v1/snapshots/config/configure", methods=["POST"])
@token_auth.login_required
def configure_snapshot():
    global snapshots

    try:
        # TODO: validate input (must be strings)
        prefix = request.json["prefix"]
        distribution = request.json["distribution"]
    except:
        return jsonify({
            "message": "Missing mandatory 'prefix' and/or 'distribution' parameter"
        }), 400

    try:
        # TODO: validate input (must be a dict)
        params = request.json["retention"]

        # TODO: validate input (must be a list of strings)
        keep = params["keep"] if "keep" in params else None
        # TODO: validate input (only numbers allowed)
        days_range = params["days_range"] if "days_range" in params else None
        weeklies_range = params["weeklies_range"] if "weeklies_range" in params else None
        monthlies_range = params["monthlies_range"] if "monthlies_range" in params else None
    except Exception as e:
        days_range = weeklies_range = monthlies_range = None

    snapshot_id = f"{prefix}:{distribution}"
    logging.info((f"Configuring snapshot for {snapshot_id}: "
                  f"keep: {keep}, "
                  f"days_range: {days_range}, "
                  f"weeklies_range: {weeklies_range}, "
                  f"monthlies_range: {monthlies_range}"))

    # TODO: actually configure snapshot

    snapshot = snapshots.get(snapshot_id)
    if not snapshot:
        snapshot = {
            "prefix": prefix,
            "distribution": distribution,
        }
        snapshots[snapshot_id] = snapshot

    if keep or days_range or weeklies_range or monthlies_range:
        if "retention" not in snapshot:
            snapshot["retention"] = {}

        for key, value in (("keep", keep),
                           ("days_range", days_range),
                           ("weeklies_range", weeklies_range),
                           ("monthlies_range", monthlies_range)):
            if value:
                snapshot["retention"][key] = value
            elif key in snapshot["retention"]:
                del snapshot["retention"][key]
    else:
        if "retention" in snapshot:
            del snapshot["retention"]

    return jsonify({})

# The prefix and distribution must be set as url params
# (e.g. http://<service_url>/v1/snapshots/config/delete?prefix=apertis&distribution=v2022).
#
# Both prefix and distribution must be an exact match against the available snapshots.
@app.route("/v1/snapshots/config/delete", methods=["DELETE"])
@token_auth.login_required
def delete_snapshot():
    global snapshots

    try:
        # TODO: validate input (must be strings)
        prefix = request.args["prefix"]
        distribution = request.args["distribution"]
    except:
        return jsonify({
            "message": "Missing mandatory 'prefix' and/or 'distribution' parameter"
        }), 400

    snapshot_id = f"{prefix}:{distribution}"
    logging.info(f"Deleting snapshot configuration for {snapshot_id}")

    if snapshot_id not in snapshots:
        return jsonify({
            "message": f"Could not delete configuration for snapshot {snapshot_id}: not found"
        }), 404

    # TODO: actually delete snapshot/configuration

    del snapshots[snapshot_id]

    return jsonify({})
