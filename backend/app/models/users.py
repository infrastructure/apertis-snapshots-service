# Apertis Snapshots Configuration Service.
#
# Copyright (C) 2022 Collabora Ltd
# Andre Moreira Magalhaes <andre.magalhaes@collabora.com>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US

from app import app, db
from werkzeug.security import check_password_hash

import datetime
import jwt
import os


class Users(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), unique=True)
    password = db.Column(db.String(60))
    name = db.Column(db.String(60), unique=True)
    email = db.Column(db.String(60))
    token = db.Column(db.String(128), unique=True)
    created_on = db.Column(db.DateTime, default=datetime.datetime.now(datetime.timezone.utc))

    def __init__(self, username, password, name, email, token=None):
        self.username = username
        self.password = password
        self.name = name
        self.email = email
        if token:
            self.token = token.encode("utf-8")
        else:
            self.generate_token()

    def generate_token(self):
        self.token = jwt.encode({
            "username": self.username,
        }, app.secret_key, algorithm="HS256")

    def verify_password(self, password):
        if app.authentication_disabled:
            return True
        return check_password_hash(self.password, password)


local_user = Users("local_user", "", "Local user", "user@local", "<bearer-token>")

def get_user_by_username(username):
    if app.authentication_disabled:
        global local_user
        return local_user

    try:
        return Users.query.filter_by(username=username).first()
    except:
        return None

def get_user_by_id(id):
    if app.authentication_disabled:
        global local_user
        return local_user

    try:
        return Users.query.filter_by(id=id).first()
    except:
        return None

def get_user_by_token(token):
    if app.authentication_disabled:
        global local_user
        return local_user

    try:
        data = jwt.decode(token, app.secret_key, algorithms='HS256')
        user = get_user_by_username(username=data['username'])
    except Exception as e:
        return None
    return user
