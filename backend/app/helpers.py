# Apertis Snapshots Configuration Service.
#
# Copyright (C) 2022 Collabora Ltd
# Andre Moreira Magalhaes <andre.magalhaes@collabora.com>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US

from app.models import get_user_by_username, get_user_by_token
from flask_httpauth import HTTPBasicAuth, HTTPTokenAuth


basic_auth = HTTPBasicAuth()
token_auth = HTTPTokenAuth(scheme='Bearer')

@basic_auth.verify_password
def verify_password(username, password):
    user = get_user_by_username(username)
    if user and user.verify_password(password):
        return user
    return None

@token_auth.verify_token
def verify_token(token):
    return get_user_by_token(token)
