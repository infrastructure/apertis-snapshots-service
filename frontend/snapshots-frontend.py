#!/usr/bin/env python3
#
# Apertis Snapshots Configuration Frontend.
#
# Copyright (C) 2022 Collabora Ltd
# Andre Moreira Magalhaes <andre.magalhaes@collabora.com>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US

from app import app
from app.manager import SnapshotManager
from app.blueprints import create_gitlab_auth_blueprint
from config import config

import argparse
import logging
import os


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Apertis Snapshots Configuration Frontend")
    parser.add_argument("-d", "--debug", action="store_true", default=False,
                        help="print more debugging info")
    parser.add_argument("-c", "--config", metavar="CONFIG",
                         help="configuration file")
    parser.add_argument('--disable-authentication', action='store_true', default=False,
                        help="local development session")
    args = parser.parse_args()

    logging.basicConfig(
        level=(logging.INFO if not args.debug else logging.DEBUG),
        format="%(asctime)s %(levelname)-8s: %(message)s",
        handlers=[
            logging.StreamHandler()
        ]
    )

    config.load_config(args)
    app.snapshot_manager = SnapshotManager(os.getenv("SNAPSHOTS_SERVICE_URL"),
            config["auth-groups"])
    app.authentication_disabled = args.disable_authentication

    # GitLab OAuth configuration
    if not app.authentication_disabled:
        blueprint = create_gitlab_auth_blueprint(config["gitlab-url"])
        app.register_blueprint(blueprint, url_prefix='/login')

    app.run("0.0.0.0", config["port"])
