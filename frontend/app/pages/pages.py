#!/usr/bin/env python3
#
# Apertis Snapshots Configuration Frontend.
#
# Copyright (C) 2022 Collabora Ltd
# Andre Moreira Magalhaes <andre.magalhaes@collabora.com>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US

from flask import render_template


def generate_index(user_info=None, snapshots=None):
    return render_template('index.html', user_info=user_info, snapshots=snapshots.values() if snapshots else [])

def generate_configure_snapshot_page(user_info, form=None):
    return render_template('configure.html', user_info=user_info, form=form)
