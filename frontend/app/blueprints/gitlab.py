# Apertis Snapshots Configuration Frontend.
#
# Copyright (C) 2022 Collabora Ltd
# Andre Moreira Magalhaes <andre.magalhaes@collabora.com>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US

from app import app
from flask import redirect, session, url_for
from flask_dance.consumer import oauth_authorized
from flask_dance.contrib.gitlab import make_gitlab_blueprint, gitlab
from requests.auth import HTTPBasicAuth
from urllib.parse import urlparse

import logging
import os
import requests


def create_gitlab_auth_blueprint(gitlab_url):
    gitlab_bp = make_gitlab_blueprint(
        client_id = os.getenv("GITLAB_OAUTH_CLIENT_ID"),
        client_secret = os.getenv("GITLAB_OAUTH_CLIENT_SECRET"),
        scope = "openid",
        hostname = urlparse(gitlab_url).netloc
    )

    @app.route('/login')
    def login():
        if not gitlab.authorized:
            return redirect(url_for("gitlab.login"))
        return redirect(url_for("index"))

    @app.route('/logout')
    def logout():
        if "user_info" not in session:
            return redirect(url_for("index"))

        access_token = session["user_access_token"]
        payload = {
            "token": access_token,
            "token_type_hint": "refresh_token"
        }
        client_id = os.getenv("GITLAB_OAUTH_CLIENT_ID")
        client_secret = os.getenv("GITLAB_OAUTH_CLIENT_SECRET")
        auth = HTTPBasicAuth(client_id, client_secret)
        response = requests.post("https://gitlab.apertis.org/oauth/revoke",
                data=payload, auth=auth)
        if response.ok:
            del session["user_access_token"]
            del session["user_info"]
        return redirect(url_for("index"))

    @oauth_authorized.connect_via(gitlab_bp)
    def redirect_after_auth(blueprint, token):
        # See https://docs.gitlab.com/ee/integration/openid_connect_provider.html
        user_info = blueprint.session.get("/oauth/userinfo").json()
        username = user_info["nickname"]
        session["user_access_token"] = token
        session["user_info"] = user_info
        logging.info(f"Logged in as '{username}'")
        return redirect(url_for("index"))

    return gitlab_bp
