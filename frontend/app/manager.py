#!/usr/bin/env python3
#
# Apertis Snapshots Configuration Frontend.
#
# Copyright (C) 2022 Collabora Ltd
# Andre Moreira Magalhaes <andre.magalhaes@collabora.com>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US

from flask import jsonify

import logging
import re
import requests
import urllib.parse


SNAPSHOTS_CONFIG_LIST_ENDPOINT = "/v1/snapshots/config/list"
SNAPSHOTS_CONFIG_LIST_ENDPOINT_PARAMS = "prefix={prefix}&distribution={distribution}"
SNAPSHOTS_CONFIG_GET_ENDPOINT = "/v1/snapshots/config/get"
SNAPSHOTS_CONFIG_GET_ENDPOINT_PARAMS = "prefix={prefix}&distribution={distribution}"
SNAPSHOTS_CONFIG_CONFIGURE_ENDPOINT = "/v1/snapshots/config/configure"
SNAPSHOTS_CONFIG_DELETE_ENDPOINT = "/v1/snapshots/config/delete"
SNAPSHOTS_CONFIG_DELETE_ENDPOINT_PARAMS = "prefix={prefix}&distribution={distribution}"

class EndpointError(Exception):
    pass

class BearerAuth(requests.auth.AuthBase):
    def __init__(self, token):
        self.token = token

    def __call__(self, request):
        request.headers["Authorization"] = f"Bearer {self.token}"
        return request

class Snapshot:

    def __init__(self, prefix, distribution, keep,
                 days_range, weeklies_range, monthlies_range):
        self.prefix = prefix
        self.distribution = distribution
        self.retention = {
            "keep": keep or [],
            "days_range": int(days_range) or 0,
            "weeklies_range": int(weeklies_range) or 0,
            "monthlies_range": int(monthlies_range) or 0
        }

    def __repr__(self):
        return (f"{{'prefix': '{self.prefix}', "
                f"'distribution': '{self.distribution}', "
                f"'retention': '{self.retention}'")

class SnapshotManager:

    def __init__(self, backend_url, allowed_auth_groups):
        self.backend_url = backend_url
        self.allowed_auth_groups = allowed_auth_groups

    def get_snapshots_config(self, user_auth_groups):
        snapshots_config = []
        for auth_group in user_auth_groups:
            if auth_group in self.allowed_auth_groups:
                auth_group_config = self.allowed_auth_groups[auth_group]
                bearer_token = auth_group_config["bearer_token"]
                for prefix in auth_group_config["allowed"]:
                    for distribution in auth_group_config["allowed"][prefix]:
                        params = SNAPSHOTS_CONFIG_LIST_ENDPOINT_PARAMS.format(
                                prefix=urllib.parse.quote_plus(prefix),
                                distribution=urllib.parse.quote_plus(distribution))
                        data = self._get(SNAPSHOTS_CONFIG_LIST_ENDPOINT, params, bearer_token)
                        snapshots_config.extend(data["snapshots"])

        snapshots = {}
        for snapshot_config in snapshots_config:
            prefix = snapshot_config["prefix"]
            distribution = snapshot_config["distribution"]
            retention = snapshot_config.get("retention") or {}
            keep = retention.get("keep", [])
            days_range = retention.get("days_range", 0)
            weeklies_range = retention.get("weeklies_range", 0)
            monthlies_range = retention.get("monthlies_range", 0)

            snapshot = Snapshot(prefix, distribution, keep,
                days_range, weeklies_range, monthlies_range)
            snapshots[f"{prefix}:{distribution}"] = snapshot

        return snapshots

    def get_snapshot_config(self, user_auth_groups, prefix, distribution):
        bearer_token = self._get_bearer_token_for_snapshot(user_auth_groups, prefix, distribution)
        if not bearer_token:
            raise PermissionError((f"The current user is not allowed to delete snapshots "
                                   f"with prefix '{prefix}' and distribution '{distribution}'"))
            return False

        params = SNAPSHOTS_CONFIG_GET_ENDPOINT_PARAMS.format(
                prefix=urllib.parse.quote_plus(prefix),
                distribution=urllib.parse.quote_plus(distribution))
        data = self._get(SNAPSHOTS_CONFIG_GET_ENDPOINT, params, bearer_token)
        snapshot_config = data["snapshot"]

        prefix = snapshot_config["prefix"]
        distribution = snapshot_config["distribution"]
        retention = snapshot_config.get("retention") or {}
        keep = retention.get("keep", [])
        days_range = retention.get("days_range", 0)
        weeklies_range = retention.get("weeklies_range", 0)
        monthlies_range = retention.get("monthlies_range", 0)

        snapshot = Snapshot(prefix, distribution, keep,
            days_range, weeklies_range, monthlies_range)

        return snapshot

    def delete_snapshot_config(self, user_auth_groups, prefix, distribution):
        bearer_token = self._get_bearer_token_for_snapshot(user_auth_groups, prefix, distribution)
        if not bearer_token:
            raise PermissionError((f"The current user is not allowed to delete snapshots "
                                   f"with prefix '{prefix}' and distribution '{distribution}'"))
            return False

        params = SNAPSHOTS_CONFIG_DELETE_ENDPOINT_PARAMS.format(
                prefix=urllib.parse.quote_plus(prefix),
                distribution=urllib.parse.quote_plus(distribution))
        data = self._delete(SNAPSHOTS_CONFIG_DELETE_ENDPOINT, params, bearer_token)
        return True

    def configure_snapshot_config(self, user_auth_groups, prefix, distribution,
                                  keep, days_range, weeklies_range, monthlies_range):
        bearer_token = self._get_bearer_token_for_snapshot(user_auth_groups, prefix, distribution)
        if not bearer_token:
            raise PermissionError((f"The current user is not allowed to configure snapshots "
                                   f"with prefix '{prefix}' and distribution '{distribution}'"))

        snapshot = Snapshot(prefix, distribution, keep, days_range, weeklies_range, monthlies_range)
        data = self._post(SNAPSHOTS_CONFIG_CONFIGURE_ENDPOINT, snapshot.__dict__, bearer_token)
        return True

    def is_prefix_allowed(self, user_auth_groups, snapshot_prefix):
        allowed = False
        for auth_group in user_auth_groups:
            if auth_group in self.allowed_auth_groups:
                auth_group_config = self.allowed_auth_groups[auth_group]
                bearer_token = auth_group_config["bearer_token"]
                for prefix in auth_group_config["allowed"]:
                    if prefix == snapshot_prefix:
                        allowed = True
                        break
        return allowed

    def is_distribution_allowed(self, user_auth_groups, snapshot_prefix, snapshot_distribution):
        allowed = False
        for auth_group in user_auth_groups:
            if auth_group in self.allowed_auth_groups:
                auth_group_config = self.allowed_auth_groups[auth_group]
                for prefix in auth_group_config["allowed"]:
                    for distribution in auth_group_config["allowed"][prefix]:
                        if prefix == snapshot_prefix and re.fullmatch(distribution, snapshot_distribution):
                            allowed = True
                            break
        return allowed

    def _get_bearer_token_for_snapshot(self, user_auth_groups, snapshot_prefix, snapshot_distribution):
        allowed = False
        bearer_token = None
        for auth_group in user_auth_groups:
            if auth_group in self.allowed_auth_groups:
                auth_group_config = self.allowed_auth_groups[auth_group]
                bearer_token = auth_group_config["bearer_token"]
                for prefix in auth_group_config["allowed"]:
                    for distribution in auth_group_config["allowed"][prefix]:
                        if prefix == snapshot_prefix and re.fullmatch(distribution, snapshot_distribution):
                            allowed = True
                            break

        if allowed:
            return bearer_token
        return None

    def _request(self, method, endpoint, bearer_token, params=None, json=None):
        url = f"{self.backend_url}{endpoint}"
        headers=None
        if params:
            logging.debug(f"Sending '{method}' request to {url}?{params}")
        else:
            logging.debug(f"Sending '{method}' request to {url}: {json}")
            headers = {"Content-Type":"application/json"}
        bearer_auth = BearerAuth(bearer_token)
        response = requests.request(method, url, params=params, headers=headers, auth=bearer_auth, json=json)
        logging.debug(f"Got response: {response}")
        if not response.ok:
            raise EndpointError(f"Error sending request to {url}: status_code: {response.status_code}, response: {response.json()}")
        data = response.json()
        logging.debug(f"Response data: {data}")
        return data

    def _get(self, endpoint, params, bearer_token):
        return self._request("GET", endpoint, bearer_token, params=params)

    def _delete(self, endpoint, params, bearer_token):
        return self._request("DELETE", endpoint, bearer_token, params=params)

    def _post(self, endpoint, json, bearer_token):
        return self._request("POST", endpoint, bearer_token, json=json)
