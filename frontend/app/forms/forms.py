#!/usr/bin/env python3
#
# Apertis Snapshots Configuration Frontend.
#
# Copyright (C) 2022 Collabora Ltd
# Andre Moreira Magalhaes <andre.magalhaes@collabora.com>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US

from app import app
from flask import session
from flask_wtf import FlaskForm
from wtforms import Form, FormField, IntegerField, SelectField, StringField, validators
from wtforms.validators import DataRequired, NumberRange, ValidationError


class SnapshotKeepRangeRetentionForm(Form):
    days = IntegerField("Keep all snapshots for the past number of days", default=0,
                        validators=[NumberRange(min=0)])
    weeklies = IntegerField("Keep weekly snapshots for the past number of weeks", default=0,
                            validators=[NumberRange(min=0)])
    monthlies = IntegerField("Keep monthly snapshots for the past number of months", default=0,
                             validators=[NumberRange(min=0)])

class SnapshotKeepManualRetentionForm(Form):
    selected = StringField("Snapshots to keep",
                           description="List of <distribution/snapshot> pairs to keep")
    ranges = FormField(SnapshotKeepRangeRetentionForm)

    def validate_selected(form, field):
        if not field.data:
            return

        user_auth_groups = session["user_info"]["groups"]
        snapshots_to_keep = [ snapshot.strip() for snapshot in field.data.split(",") ]
        for snapshot in snapshots_to_keep:
            snapshot_info = snapshot.split("/")
            if len(snapshot_info) != 2:
                raise ValidationError(f"Snapshot pattern must match <distribution/snapshot>")

            distribution_pattern, snapshot_pattern = snapshot_info
            # TODO: actually validate distribution_pattern and snapshot_pattern


class SnapshotForm(FlaskForm):
    prefix = StringField("Prefix",
                         validators=[DataRequired(), validators.Regexp(regex="^\S+$")],
                         description="Prefix")
    distribution = StringField("Distribution",
                               validators=[DataRequired(), validators.Regexp(regex="^\S+$")],
                               description="Distribution")
    keep_type = SelectField("Retention",
                            choices=[("keep-all", "Keep all snapshots"),
                                     ("keep-manual", "Keep selected snapshots")])
    keep_manual = FormField(SnapshotKeepManualRetentionForm)

    def validate_prefix(form, field):
        user_auth_groups = session["user_info"]["groups"]
        prefix = field.data.strip()
        if not app.snapshot_manager.is_prefix_allowed(user_auth_groups, prefix):
            raise ValidationError(f"User is not allowed to configure this prefix")

    def validate_distribution(form, field):
        user_auth_groups = session["user_info"]["groups"]
        prefix = form.prefix.data.strip()
        distribution_pattern = field.data.strip()
        if not app.snapshot_manager.is_distribution_allowed(user_auth_groups, prefix, distribution_pattern):
            raise ValidationError(f"User is not allowed to configure distribution \"{distribution_pattern}\" with requested prefix \"{form.prefix.data}\"")
