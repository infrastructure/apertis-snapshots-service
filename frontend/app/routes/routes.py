# Apertis Snapshots Configuration Frontend.
#
# Copyright (C) 2022 Collabora Ltd
# Andre Moreira Magalhaes <andre.magalhaes@collabora.com>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US

from app import app
from app.pages import generate_index, generate_configure_snapshot_page
from flask import redirect, request, session, url_for

from app.forms import SnapshotForm

import logging


local_user_info = {
    "nickname": "local_user",
    "name": "Local user",
    "email": "user@localhost",
    "profile": "#",
    "groups": [ "apertis-developers", "apertis-maintainers" ],
    "local": True
}

@app.route('/', methods=['GET'])
def index():
    if app.authentication_disabled and 'user_info' not in session:
        global local_user_info
        session['user_info'] = local_user_info

    try:
        snapshots = None
        if "user_info" in session:
            user_auth_groups = session["user_info"]["groups"]
            snapshots = app.snapshot_manager.get_snapshots_config(user_auth_groups)
        return generate_index(user_info=session.get("user_info"), snapshots=snapshots)
    except Exception as e:
        logging.error(e)

@app.route('/configure/<prefix>/<distribution>', methods=['GET', 'POST'])
@app.route('/configure', methods=['GET', 'POST'])
def configure(prefix=None, distribution=None):
    if not app.authentication_disabled and "user_info" not in session:
        return redirect(url_for("index"))

    try:
        form = SnapshotForm()
        if form.validate_on_submit():
            user_auth_groups = session["user_info"]["groups"]

            keep_type = request.form["keep_type"]
            if keep_type == "keep-all":
                keep = ["*"]
            else:
                keep = request.form.get("keep_manual-selected", "").strip()
                keep = keep.split(",") if len(keep) else []
            keep_ranges_days = request.form.get("keep_manual-ranges-days", 0)
            keep_ranges_weeklies = request.form.get("keep_manual-ranges-weeklies", 0)
            keep_ranges_monthlies = request.form.get("keep_manual-ranges-monthlies", 0)
            app.snapshot_manager.configure_snapshot_config(user_auth_groups,
                request.form["prefix"],
                request.form["distribution"],
                keep,
                keep_ranges_days,
                keep_ranges_weeklies,
                keep_ranges_monthlies)
            return redirect(url_for("index"))
        elif request.method == "POST":
            return generate_configure_snapshot_page(session["user_info"], form)

        snapshot = None
        if prefix and distribution:
            user_auth_groups = session["user_info"]["groups"]
            snapshot = app.snapshot_manager.get_snapshot_config(user_auth_groups,
                    prefix, distribution)
        if snapshot:
            form.prefix.data = snapshot.prefix
            form.distribution.data = snapshot.distribution
            for field in (form.prefix, form.distribution):
                if field.render_kw is None:
                    field.render_kw = {}
                field.render_kw["readonly"]= True
            keep = ', '.join(snapshot.retention["keep"])
            if keep in ["*"]:
                keep = ""
                form.keep_type.data = "keep-all"
            else:
                form.keep_type.data = "keep-manual"
            form.keep_manual.prefix = form.prefix
            form.keep_manual.selected.data = keep
            form.keep_manual.ranges.days.data = snapshot.retention["days_range"]
            form.keep_manual.ranges.weeklies.data = snapshot.retention["weeklies_range"]
            form.keep_manual.ranges.monthlies.data = snapshot.retention["monthlies_range"]
        return generate_configure_snapshot_page(session["user_info"], form)
    except Exception as e:
        logging.error(e)

@app.route('/delete/<prefix>/<distribution>', methods=['GET'])
def delete(prefix, distribution):
    if not app.authentication_disabled and "user_info" not in session:
        return redirect(url_for("index"))

    try:
        user_auth_groups = session["user_info"]["groups"]
        app.snapshot_manager.delete_snapshot_config(user_auth_groups, prefix, distribution)
        return redirect(url_for("index"))
    except Exception as e:
        logging.error(e)
